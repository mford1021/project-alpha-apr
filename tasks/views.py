from django.shortcuts import render, get_object_or_404, redirect
from tasks.models import Task
from django.contrib.auth.decorators import login_required
from tasks.forms import TaskForm

# Create your views here.


@login_required
def project(request):
    projects = Task.objects.filter(owner=request.user)
    context = {"projects": projects}
    return render(request, "projects/home.html", context)


@login_required
def show_task(request, id):
    task = get_object_or_404(Task, id=id)
    context = {"task_object": task}
    return render(request, "projects/detail.html", context)


@login_required
def create_task(request):
    if request.method == "POST":
        form = TaskForm(request.POST)
        if form.is_valid():
            project = form.save()
            project.save()
            return redirect("/projects/")
    else:
        form = TaskForm()
    context = {"form": form}
    return render(request, "projects/create-task.html", context)


@login_required
def show_my_tasks(request):
    tasks = Task.objects.filter(assignee=request.user)
    context = {"tasks": tasks}
    return render(request, "tasks/mine.html", context)
