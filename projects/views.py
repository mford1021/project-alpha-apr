from django.shortcuts import render, redirect
from projects.models import Project
from django.contrib.auth.decorators import login_required
from tasks.models import Task
from projects.forms import ProjectForm

# Create your views here.


@login_required
def project(request):
    projects = Project.objects.filter(owner=request.user)
    context = {"projects": projects}
    return render(request, "projects/home.html", context)


@login_required
def show_project(request, id):
    tasks = Task.objects.filter(id=id)
    context = {"tasks": tasks}
    return render(request, "projects/detail.html", context)


@login_required
def create_project(request):
    if request.method == "POST":
        form = ProjectForm(request.POST)
        if form.is_valid():
            project = form.save(False)
            project.assignee = request.user
            project.save()
            return redirect("/projects/")
    else:
        form = ProjectForm()
    context = {"form": form}
    return render(request, "projects/create.html", context)
